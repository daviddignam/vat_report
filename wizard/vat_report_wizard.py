# -*- coding: utf-8 -*-

from openerp import models, fields, api
import logging

_logger = logging.getLogger(__name__)


class TranslationieVatReportWizard(models.TransientModel):
	_name = 'wizard.vat.report'
	company_id = fields.Many2one('res.company', 'Company', required=True, default=lambda self: self.env.user.company_id)
	from_date = fields.Date('From date', required=True)
	to_date = fields.Date('To date', required=True)
		
	@api.multi
	def report_vat(self):
		try:
			_logger.debug("Reporting VAT")
			if not self.company_id.tax_cash_basis_journal_id:
				raise UserError(_('There is no tax cash basis journal defined ' \
										'for this company: "%s" \nConfigure it in Accounting/Configuration/Settings') % \
										(self.company_id.name))
			cash_basis_journal = self.company_id.tax_cash_basis_journal_id
			vat_cash_basis_moves = self.env['account.move'].search([('journal_id','=',cash_basis_journal.id),('date','>=',self.from_date),('date','<=',self.to_date),('state','=','posted'),('is_vat_move','=',True)])
			_logger.debug("cash basis moves: " + str(vat_cash_basis_moves))
			sale_cash_basis_move_ids = []
			purchase_cash_basis_move_ids = []
			total_sales_vatable = 0
			total_sales_vat = 0
			total_purchases_vatable = 0
			total_purchases_vat = 0
			for move in vat_cash_basis_moves:
				#categorise cash_basis_moves to sales or purchases based on Originator tax type (tax_line_id)
				if any([line.tax_line_id.type_tax_use == 'sale' for line in move.line_ids]):
					total_sales_vatable, total_sales_vat = self._add_vat_totals(move, total_sales_vatable, total_sales_vat, 'sale')
					sale_cash_basis_move_ids.extend([line.id for line in move.line_ids])
				elif any([line.tax_line_id.type_tax_use == 'purchase' for line in move.line_ids]):
					total_purchases_vatable, total_purchases_vat = self._add_vat_totals(move, total_purchases_vatable, total_purchases_vat, 'purchase')
					purchase_cash_basis_move_ids.extend([line.id for line in move.line_ids])
			
			intra_eu_sale_move_ids = []
			total_intra_eu_sale = 0
			intra_eu_purchase_move_ids = []
			total_intra_eu_purchase = 0
			intra_eu_cash_basis_moves = self.env['account.move'].search([('journal_id','=',cash_basis_journal.id),('date','>=',self.from_date),('date','<=',self.to_date),('state','=','posted'),('is_intra_eu_move','=',True)])
			
			for move in intra_eu_cash_basis_moves:
				_logger.debug("move is: " + str(move.name))
				for line in move.line_ids:
					_logger.debug("taxes: " + str(line.tax_ids))
					for tax in line.tax_ids:
						_logger.debug("tax type: " + str(tax.type_tax_use))
				if any([tax.type_tax_use == 'sale' for tax in line.tax_ids for line in move.line_ids]):
					_logger.debug("Appending intra EU sale")
					total_intra_eu_sale = self._add_base_total(move, total_intra_eu_sale, 'sale')
					intra_eu_sale_move_ids.extend([line.id for line in move.line_ids])
				elif any([tax.type_tax_use == 'purchase' for tax in line.tax_ids for line in move.line_ids]):
					_logger.debug("Appending intra EU purchase")
					total_intra_eu_purchase = self._add_base_total(move, total_intra_eu_purchase, 'purchase')
					intra_eu_purchase_move_ids.extend([line.id for line in move.line_ids])
			_logger.debug("intra eu sale moves: " + str(intra_eu_sale_move_ids))
			_logger.debug("intra eu purchase moves: " + str(intra_eu_purchase_move_ids))
			data = {
				'sales_vatable_amnt': total_sales_vatable,
				'sale_vat_amnt_t1': total_sales_vat,
				'purchases_vatable_amnt': total_purchases_vatable,
				'purchase_vat_amnt_t2' : total_purchases_vat,
				'eu_service_sale_amnt_es1': total_intra_eu_sale,
				'eu_service_purchase_amnt_es2': total_intra_eu_purchase,
			}
			_logger.debug("sale moves: " + str(sale_cash_basis_move_ids))
			if len(sale_cash_basis_move_ids) > 0:
				data['sale_vat_moves'] = [(4, [id for id in sale_cash_basis_move_ids])]
			if len(purchase_cash_basis_move_ids) > 0:
				data['purchase_vat_moves'] = [(4, [id for id in purchase_cash_basis_move_ids])]
			if len(intra_eu_sale_move_ids) > 0:
				data['eu_service_sale_moves'] = [(4, [id for id in intra_eu_sale_move_ids])]
			if len(intra_eu_purchase_move_ids) > 0:
				data['eu_service_purchase_moves'] = [(4, [id for id in intra_eu_purchase_move_ids])]
			_logger.debug("Total Vatable Sale: " + str(total_sales_vatable) + " Total Sale VAT: " + str(total_sales_vat) + " Sale Moves: " + str(sale_cash_basis_move_ids))
			_logger.debug("Total Vatable Purchase: " + str(total_purchases_vatable) + " Total Purchase VAT: " + str(total_purchases_vat) + " Purchase Moves: " + str(purchase_cash_basis_move_ids))
			
			vat_report_obj = self.env['vat.report'].create(data)
			
			treeview_id = self.env.ref('translationie_vat_report.view_vat_report')
			
			return {
				'name': 'VAT Report View',
				'type': 'ir.actions.act_window',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'res_model': 'vat.report',
				'views': [(treeview_id.id, 'tree')],
				'target': 'current',
				'domain': [('id','=',vat_report_obj.id)],
			}
		
		except Exception:
			_logger.error("Error reporting VAT", exc_info=True)
		
	
	def _add_base_total(self, move, total_base, type):
		base_lines = [line for line in move.line_ids if line.tax_ids]
		for line in base_lines:
			if line.credit > 0:
				# for sale increase value of sales for which we received vat
				if type == 'sale':
					total_base += line.credit
				# for purchase decrease value of purchases for which we paid vat
				elif type == 'purchase':
					total_base -= line.credit
			elif line.debit > 0:
				# for sale decrease value of sales for which we received vat
				if type == 'sale':
					total_base -= line.debit
				# for purchase increase value of purchases for which we paid vat
				elif type == 'purchase':
					total_base += line.debit
			_logger.debug("base_total: " + str(total_base))
		return total_base
	
	def _add_vat_totals(self, move, total_vatable, total_vat, type):
		tax_line = [line for line in move.line_ids if line.tax_line_id][0]
		
		#for sales if value is credit then we're increasing the amount we received and the tax we owe the authorities,
		# if value is debit it's a reversal for a refunded sale or other reason and we're decreasing the amount we received and the tax as we no longer owe it.
		if tax_line.credit > 0:
			# for sale increase tax we received and thus owe to government
			if type == 'sale':
				total_vat += tax_line.credit
			# for purchase decrease tax we paid to vendors
			elif type =='purchase':
				total_vat -= tax_line.credit
				
		elif tax_line.debit > 0:
			# for sale decrease tax we received and thus owe to government
			if type == 'sale':
				total_vat -= tax_line.debit
			# for purchase increase tax we paid to vendors
			elif type == 'purchase':
				total_vat += tax_line.debit
		_logger.debug(type + " total_vat: " + str(total_vat))
		
		total_vatable = self._add_base_total(move, total_vatable, type)
		
		_logger.debug(type + " total_vatable: " + str(total_vatable))
		return total_vatable, total_vat

		
			