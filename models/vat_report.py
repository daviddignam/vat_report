# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)


class TranslationieVatReport(models.TransientModel):
	_name = 'vat.report'
	
	currency_id = fields.Many2one('res.currency', string='Currency')
	
	#collection of journal entries which the total sales vatable and total sales vat(T1) is calculated from for the period
	sale_vat_moves = fields.Many2many(comodel_name='account.move.line', relation='sale_vat_moves_rel')
	#Monetary value of sales for which vat must be paid
	sales_vatable_amnt = fields.Monetary(string="Sales VATable")
	#T1 amount for vat on sales
	sale_vat_amnt_t1 = fields.Monetary(string="Sales VAT")
	
	#collection of journal entries which the total purchases vatable and total purchases vat(T2) is calculated from for the period
	purchase_vat_moves = fields.Many2many(comodel_name='account.move.line', relation='purchase_vat_moves_rel')
	##Monetary value of purchases for which vat must be paid
	purchases_vatable_amnt = fields.Monetary(string="Purchases VATable")
	#T2 amount for vat on purchases
	purchase_vat_amnt_t2 = fields.Monetary(string="Purchases VAT")
	
	#collection of journal entries which the total services sold to customers in other EU countries is calculated from for the period
	eu_service_sale_moves = fields.Many2many(comodel_name='account.move.line', relation='es1_moves_rel')
	#ES1 amount for sales to other EU countries
	eu_service_sale_amnt_es1 = fields.Monetary(string="Intra-EU Sales")
	
	#collection of journal entries which the total services from from vendors in other EU countries is calculated from for the period
	eu_service_purchase_moves = fields.Many2many(comodel_name='account.move.line', relation='es2_moves_rel')
	#ES2 amount for purchases from other EU countries
	eu_service_purchase_amnt_es2 = fields.Monetary(string="Intra-EU Purchases")
	
	@api.multi
	def open_journal_entries(self, moves):
		try:
			all = self.env['vat.report'].browse([])
			_logger.debug("all items: " + str(all) + " self: " + str(self.id))
			_logger.debug("in open journal items: " + str(moves))
			action = self.env.ref('account.action_account_moves_all_tree')
			vals = action.read()[0]
			vals['context'] = {'group_by':'move_id'}
			vals['domain'] = [('id','in',moves.ids)]
			return vals
		except Exception:
			_logger.error("error opening record", exc_info=True)
	
	@api.multi
	def view_sale_moves(self):
		return self.open_journal_entries(self.sale_vat_moves)
	
	@api.multi
	def view_purchase_moves(self):
		return self.open_journal_entries(self.purchase_vat_moves)
	
	@api.multi
	def view_eu_service_sale_moves(self):
		return self.open_journal_entries(self.eu_service_sale_moves)
	
	@api.multi
	def view_eu_service_purchase_moves(self):
		return self.open_journal_entries(self.eu_service_purchase_moves)

class AccountTax(models.Model):
	_inherit = 'account.tax'
	
	is_vat = fields.Boolean(string="Is VAT", help="This tax is a value added tax (VAT) and will be considered while generating the VAT3 report")
	is_intra_eu_vat_exempt = fields.Boolean(string="VAT Exempt Intra-EU")
	
	@api.constrains('is_vat', 'is_intra_eu_vat')
	def _check_vat_status(self):
		if self.is_vat and self.is_intra_eu_vat_exempt:
			raise ValidationError("Cannot select both 'is_vat' and 'is_intra_eu_vat' for a tax object")