# -*- coding: utf-8 -*-

from openerp import models, fields, api, _
from openerp.exceptions import UserError
import logging

_logger = logging.getLogger(__name__)

class AccountPartialReconcileCashBasis(models.Model):
	_inherit = 'account.partial.reconcile'
	
	def create_tax_cash_basis_entry(self, value_before_reconciliation):
		try:
			#Search in account_move if we have any taxes account move lines
			tax_group = {}
			tax_group_lines = {}
			tax_group_date = {}
			total_by_cash_basis_account = {}
			cash_basis_date = {}
			base_line_to_create = []
			move_date = self.debit_move_id.date
			newly_created_move = self.env['account.move']
			
			#mark move as vat move for vat reporting at later stage
			is_vat_move = False
			is_intra_eu_move = False
			for move in (self.debit_move_id.move_id, self.credit_move_id.move_id):
				if any([line.tax_line_id.is_vat for line in move.line_ids]):
					_logger.debug("Setting Move is Vat True")
					is_vat_move = True
				else:
					_logger.debug("Setting Move is Vat False")
					is_vat_move = False
					is_intra_eu_move = False
				if move_date < move.date:
					move_date = move.date
				for line in move.line_ids:
					# if move is actually intra-eu transaction then mark as so for segregation into Intra-EU Supply of Services Number (ES1 in Irish VAT3 Report)
					if any([tax_id.is_intra_eu_vat_exempt for tax_id in line.tax_ids]):
						_logger.debug("Setting Move is Intra-EU True")
						is_vat_move = False
						is_intra_eu_move = True
					#amount to write is the current cash_basis amount minus the one before the reconciliation
					matched_percentage = value_before_reconciliation[move.id]
					amount = (line.credit_cash_basis - line.debit_cash_basis) - (line.credit - line.debit) * matched_percentage
					rounded_amt = line.company_id.currency_id.round(amount)
					if line.tax_line_id and line.tax_line_id.use_cash_basis:
						if not newly_created_move:
							newly_created_move = self._create_tax_basis_move(is_vat_move, is_intra_eu_move)
						#group by line account
						acc = line.account_id.id
						if tax_group.get(acc, False):
							tax_group[acc] += amount
						else:
							tax_group[acc] = amount
						
						if tax_group_lines.get(acc, False):
							tax_group_lines[acc].append(line)
						else:
							tax_group_lines[acc] = [line]
						#get line due date (which should be the date the money was received for cash based vat)
						if len(line.invoice_id.payment_ids) > 1:
							date = line.invoice_id.payment_ids[0].payment_date
						else:
							date = line.invoice_id.payment_ids.payment_date
						tax_group_date[acc] = date
						
						#Group by cash basis account
						acc = line.tax_line_id.cash_basis_account.id
						if total_by_cash_basis_account.get(acc, False):
							total_by_cash_basis_account[acc] += amount
						else:
							total_by_cash_basis_account[acc] = amount
						cash_basis_date[acc] = date
						if tax_group_lines.get(acc, False):
							tax_group_lines[acc].append(line)
						else:
							tax_group_lines[acc] = [line]
							
					#create cash basis entry for base (Base Transaction less VAT)
					if any([tax.use_cash_basis for tax in line.tax_ids]):
						if not newly_created_move:
							newly_created_move = self._create_tax_basis_move(is_vat_move, is_intra_eu_move)
						for tax in line.tax_ids:
							#assign tax_ids to actual debit/credit line for proper accounting in VAT report later
							#IMPORTANT: call create with context dont_create_taxes=True to avoid quirk of system creating another tax entry for what is already a cash basis tax entry
							#effectively recording vat twice
							self.env['account.move.line'].with_context(check_move_validity=False, dont_create_taxes=True).create({
								'name': line.name,
								'debit': abs(rounded_amt) if rounded_amt < 0 else 0.0,
								'credit': rounded_amt if rounded_amt > 0 else 0.0,
								'account_id': line.account_id.id,
								'tax_ids': [(6, 0, [tax.id])],
								'currency_id': line.currency_id.id,
								'amount_currency': self.amount_currency and line.currency_id.round(line.amount_currency * amount / line.balance) or 0.0,
								'move_id': newly_created_move.id,
								'partner_id': line.partner_id.id,
								})
							#create counterpart base entry as required to balance journal entry. 
							#We don't actually care about this value for the purpose of accounting VAT but required due to nature of double entry system
							self.env['account.move.line'].with_context(check_move_validity=False, dont_create_taxes=True).create({
								'name': line.name,
								'debit': rounded_amt > 0 and rounded_amt or 0.0,
								'credit': rounded_amt < 0 and abs(rounded_amt) or 0.0,
								'account_id': line.account_id.id,
								'currency_id': line.currency_id.id,
								'amount_currency': self.amount_currency and line.currency_id.round(-line.amount_currency * amount / line.balance) or 0.0,
								'move_id': newly_created_move.id,
								'partner_id': line.partner_id.id,
								})
			#Create counterpart VAT entry in VAT Control account to balance against entry recorded at Sale
			for k,v in tax_group.items():
				line = tax_group_lines[k][0]
				
				self.env['account.move.line'].with_context(check_move_validity=False).create({
					'name': line.move_id.name,
					'debit': v if v > 0 else 0.0,
					'credit': abs(v) if v < 0 else 0.0,
					'account_id': k,
					'date_maturity': tax_group_date[k],
					'date': tax_group_date[k],
					'move_id': newly_created_move.id,
					'partner_id': line.partner_id.id,
					})

			#Create VAT entry in VAT received account
			for k,v in total_by_cash_basis_account.items():
				line = tax_group_lines[k][0]
				
				self.env['account.move.line'].with_context(check_move_validity=False).create({
					'name': line.name,
					'debit': abs(v) if v < 0 else 0.0,
					'credit': v if v > 0 else 0.0,
					'account_id': k,
					'date_maturity': cash_basis_date[k],
					'date': cash_basis_date[k],
					'partner_id': line.partner_id.id,
					'move_id': newly_created_move.id,
					'tax_line_id': line.tax_line_id.id,
					})
			
			
			if newly_created_move:
				if move_date > (self.company_id.period_lock_date or '0000-00-00') and newly_created_move.date != move_date:
					# The move date should be the maximum date between payment and invoice (in case
					# of payment in advance). However, we should make sure the move date is not
					# recorded before the period lock date as the tax statement for this period is
					# probably already sent to the estate.
					newly_created_move.write({'date': move_date})
				newly_created_move.post()
		except Exception:
			_logger.error("error generating cash basis move", exc_info=True)
			
	def _create_tax_basis_move(self, is_vat_move=False, is_intra_eu_move=False):
		# Check if company_journal for cash basis is set if not, raise exception
		if not self.company_id.tax_cash_basis_journal_id:
			raise UserError(_('There is no tax cash basis journal defined '
							  'for this company: "%s" \nConfigure it in Accounting/Configuration/Settings') %
							(self.company_id.name))
		move_vals = {
			'journal_id': self.company_id.tax_cash_basis_journal_id.id,
			'tax_cash_basis_rec_id': self.id,
			'ref': self.credit_move_id.move_id.name if self.credit_move_id.payment_id else self.debit_move_id.move_id.name,
		}
		if is_vat_move:
			move_vals['is_vat_move'] = is_vat_move
		elif is_intra_eu_move:
			move_vals['is_intra_eu_move'] = is_intra_eu_move
		return self.env['account.move'].create(move_vals)
			