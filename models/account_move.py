from openerp import models, fields, api, _
from openerp.exceptions import UserError
import logging

_logger = logging.getLogger(__name__)

class AccountMove(models.Model):
	_inherit = 'account.move'
	
	is_vat_move = fields.Boolean()
	is_intra_eu_move = fields.Boolean()
	
	#In order to better record VAT we assign the tax_ids to the base line of that we care about i.e. the actual debit/credit that records the amount of sale/purchase/refund
	#the system by default tries to always create counterpart tax entries for an account.move record unless the context value dont_create_taxes is set to True
	#this is also true in the case of reversing account moves, as such we have to override the reverse_moves method to check if the move it is reversing is a cash basis tax entry
	#otherwise the system will create an additional tax entry while reversing the move and the reversal move will be unbalanced and invalid and the reversal will fail.
	@api.multi
	def reverse_moves(self, date=None, journal_id=None):
		date = date or fields.Date.today()
		reversed_moves = self.env['account.move']
		for ac_move in self:
			#check if move we're reversing is for cash basis tax, if so don't need to create taxes
			if ac_move.journal_id == self.company_id.tax_cash_basis_journal_id:
				reversed_move = ac_move.with_context(dont_create_taxes=True).copy(default={'date': date,
					'journal_id': journal_id.id if journal_id else ac_move.journal_id.id,
					'ref': _('reversal of: ') + ac_move.name})
			else:
				reversed_move = ac_move.copy(default={'date': date,
					'journal_id': journal_id.id if journal_id else ac_move.journal_id.id,
					'ref': _('reversal of: ') + ac_move.name})
			for acm_line in reversed_move.line_ids:
				acm_line.with_context(check_move_validity=False).write({
					'debit': acm_line.credit,
					'credit': acm_line.debit,
					'amount_currency': -acm_line.amount_currency
					})
			reversed_moves |= reversed_move
		if reversed_moves:
			reversed_moves._post_validate()
			reversed_moves.post()
			return [x.id for x in reversed_moves]
		return []